#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#include "../ouichefs_ioctl.h"

int main(int argc, char **argv){
    if (argc != 4)
        return 0;
    printf("%s\n", argv[1]);
    int fd = open(argv[1], 0);
    if(fd < 0)
        puts("Open err");
    
    int req = atoi(argv[2]);
    int version = atoi(argv[3]);
    ioctl(fd, req, (long)version);
    return 0;

    close(fd);

    return 0;
}
