/*
// SPDX-License-Identifier: GPL-2.0
 * ouiche_fs - a simple educational filesystem for Linux
 *
 * Copyright (C) 2018  Redha Gouicem <redha.gouicem@lip6.fr>
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/buffer_head.h>
#include <linux/list.h>
#include <linux/slab.h>

#include "ouichefs.h"
static struct dentry *debug_dir;

struct mount_list {
	int ino;
	struct dentry *dentry;
	struct list_head list;
};

static int mounts_cpt;
static struct list_head mounts_lst;

/*
* Iterates the index blocks of a file
* and fills the *buf with their numbers while
* incrementing *version.
*/
static void iter_blocks(struct super_block *sb, int blockno,
				char *buf, int bufsz, int *version)
{
	struct buffer_head *bh;
	struct ouichefs_file_index_block *blk;
	int wrote = 0, prev = -1;

	memset(buf, 0, bufsz);
	*version = 0;

	while (blockno != 0) {
		wrote += sprintf(buf + wrote, "%d, ", blockno);
		bh = sb_bread(sb, blockno);
		blk = (struct ouichefs_file_index_block *)bh->b_data;
		/* debug aditionnel pour les block de data */
		/*
		*i = 0;
		*while(blk->blocks[i] != 0){
		*	pr_info("\t\tindex block %d - data block %d\n",
		*		blockno, blk->blocks[i]);
		*	i++;
		*}
		*/
		blockno = blk->prev;

		(*version)++;
		prev = blockno;
	}

	buf[wrote - 2] = '\n';
	buf[wrote - 1] = 0;
}

ssize_t debug_read(struct file *file, char __user *ubuf,
					size_t count, loff_t *offset)
{
	struct inode *inode;
	struct mount_list *cur, *t;
	struct dentry *mount_dentry;
	struct ouichefs_sb_info *sbi;
	struct buffer_head *bh_index;
	struct ouichefs_inode_info *ci;
	struct ouichefs_file_index_block *blk;

	int i, version, wrote = 0, copied;
	char buf[256];

	if (!ubuf) {
		pr_err("user buffer is null");
		return 0;
	}
	char *kbuf = kmalloc(count, GFP_KERNEL);

	if (!kbuf)
		return -ENOMEM;

	memset(kbuf, 0, count);

	t = NULL;
	/* on trouve le mount qu'on veut debug */
	list_for_each_entry(cur, &mounts_lst, list) {
		if (cur->ino == file->f_inode->i_ino) {
			t = cur;
			break;
		}
	}
	/* le mount est pas dans la liste */
	if (t == NULL) {
		pr_info("didnt' find right lst\n");
		return count;
	}

	mount_dentry = t->dentry;
	sbi = OUICHEFS_SB(mount_dentry->d_sb);


	if (mount_dentry->d_sb == NULL) {
		pr_info("d_sb is null\n");
		return count;
	}

	list_for_each_entry(inode, &mount_dentry->d_sb->s_inodes, i_sb_list) {
		ci = OUICHEFS_INODE(inode);
		if (!S_ISREG(inode->i_mode))
			continue;

		iter_blocks(inode->i_sb, ci->last_index_block,
			buf, 256, &version);

		wrote += scnprintf(kbuf + wrote, count - wrote,
				"%9d|%9d|%s", inode->i_ino, version, buf);
	}
	copied = copy_to_user(ubuf, kbuf, wrote);
	kfree(kbuf);
	if (copied) {
		pr_err("read copy_to_user failed to copy = %d b\n", copied);
		return -EFAULT;
	}
	if (!*offset) {
		*offset += wrote;
		return wrote;
	}

ret:
	return 0;
}

static const struct file_operations log_fops = {
	.owner  =   THIS_MODULE,
	.read   =   debug_read,
    /*.write  =   log_write */ /* maybe you don't need this */
};
/*
 * Mount a ouiche_fs partition
 */
struct dentry *ouichefs_mount(struct file_system_type *fs_type, int flags,
			      const char *dev_name, void *data)
{
	char nm[16];
	struct dentry *dentry = NULL, *debug;
	struct mount_list *mnt;

	dentry = mount_bdev(fs_type, flags, dev_name, data,
			    ouichefs_fill_super);
	if (IS_ERR(dentry))
		pr_err("'%s' mount failure\n", dev_name);
	else
		pr_info("'%s' mount success\n", dev_name);

	/* exo 2, debugfs */
	mnt = kmalloc(sizeof(struct mount_list), GFP_KERNEL);
	if (!mnt)
		return dentry;

	sprintf(nm, "mount-%d", ++mounts_cpt);
	debug = debugfs_create_file(nm, 0444, debug_dir, NULL, &log_fops);
	if (!debug)
		pr_info("failed to create debug file for partition");
	else
		pr_info("created debug file for partition %s\n",
					dentry->d_parent->d_name.name);

	mnt->ino = debug->d_inode->i_ino;
	mnt->dentry = dentry;
	list_add(&mnt->list, &mounts_lst);

	return dentry;
}

/*
 * Unmount a ouiche_fs partition
 */
void ouichefs_kill_sb(struct super_block *sb)
{
	kill_block_super(sb);

	pr_info("unmounted disk\n");
}

static struct file_system_type ouichefs_file_system_type = {
	.owner = THIS_MODULE,
	.name = "ouichefs",
	.mount = ouichefs_mount,
	.kill_sb = ouichefs_kill_sb,
	.fs_flags = FS_REQUIRES_DEV,
	.next = NULL,
};

static int __init ouichefs_init(void)
{
	int ret;

	mounts_cpt = 0;
	ret = ouichefs_init_inode_cache();
	if (ret) {
		pr_err("inode cache creation failed\n");
		goto end;
	}

	ret = register_filesystem(&ouichefs_file_system_type);
	if (ret) {
		pr_err("register_filesystem() failed\n");
		goto end;
	}

	debug_dir = debugfs_create_dir("ouichefs", NULL);
	if (!debug_dir)
		pr_err("failed to create debugfs dir");

	INIT_LIST_HEAD(&mounts_lst);

	pr_info("module loaded\n");
end:
	return ret;
}

static void __exit ouichefs_exit(void)
{
	int ret;

	ret = unregister_filesystem(&ouichefs_file_system_type);
	if (ret)
		pr_err("unregister_filesystem() failed\n");

	ouichefs_destroy_inode_cache();

	debugfs_remove_recursive(debug_dir);

	pr_info("module unloaded\n");
}

module_init(ouichefs_init);
module_exit(ouichefs_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Redha Gouicem, <redha.gouicem@lip6.fr>");
MODULE_DESCRIPTION("ouichefs, a simple educational filesystem for Linux");

