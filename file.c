/*
// SPDX-License-Identifier: GPL-2.0
 * ouiche_fs - a simple educational filesystem for Linux
 *
 * Copyright (C) 2018 Redha Gouicem <redha.gouicem@lip6.fr>
 */

#define pr_fmt(fmt) "ouichefs: " fmt

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>
#include <linux/mpage.h>

#include "ouichefs.h"
#include "bitmap.h"
#include "ouichefs_ioctl.h"

/*
 * Map the buffer_head passed in argument with the iblock-th block of the file
 * represented by inode. If the requested block is not allocated and create is
 * true,  allocate a new block on disk and map it.
 */
static int ouichefs_file_get_block(struct inode *inode, sector_t iblock,
				   struct buffer_head *bh_result, int create)
{
	struct super_block *sb = inode->i_sb;
	struct ouichefs_sb_info *sbi = OUICHEFS_SB(sb);
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct ouichefs_file_index_block *index;
	struct buffer_head *bh_index;
	bool alloc = false;
	int ret = 0, bno;

	/* If block number exceeds filesize, fail */
	if (iblock >= OUICHEFS_BLOCK_SIZE >> 2)
		return -EFBIG;

	/* Read index block from disk */
	bh_index = sb_bread(sb, ci->index_block);
	if (!bh_index)
		return -EIO;
	index = (struct ouichefs_file_index_block *)bh_index->b_data;

	/*
	 * Check if iblock is already allocated. If not and create is true,
	 * allocate it. Else, get the physical block number.
	 */
	pr_debug("%s getting blk no %d real id %d, creating %d\n",
			__func__, iblock, index->blocks[iblock], create);

	if (index->blocks[iblock] == 0) {
		if (!create)
			return 0;

		bno = get_free_block(sbi);
		if (!bno) {
			ret = -ENOSPC;
			goto brelse_index;
		}
		index->blocks[iblock] = bno;
		alloc = true;
	} else {
		bno = index->blocks[iblock];
	}

	/* Map the physical block to to the given buffer_head */
	map_bh(bh_result, sb, bno);

brelse_index:
	brelse(bh_index);

	return ret;
}

/*
 * Called by the page cache to read a page from the physical disk and map it in
 * memory.
 */
static int ouichefs_readpage(struct file *file, struct page *page)
{
	return mpage_readpage(page, ouichefs_file_get_block);
}

/*
 * Called by the page cache to write a dirty page to the physical disk (when
 * sync is called or when memory is needed).
 */
static int ouichefs_writepage(struct page *page, struct writeback_control *wbc)
{
	return block_write_full_page(page, ouichefs_file_get_block, wbc);
}

/*
 * Called by the VFS when a write() syscall occurs on file before writing the
 * data in the page cache. This functions checks if the write will be able to
 * complete and allocates the necessary blocks through block_write_begin().
 */
static int ouichefs_write_begin(struct file *file,
				struct address_space *mapping, loff_t pos,
				unsigned int len, unsigned int flags,
				struct page **pagep, void **fsdata)
{
	struct ouichefs_sb_info *sbi = OUICHEFS_SB(file->f_inode->i_sb);
	int err, syncok;
	uint32_t nr_allocs = 0;

	uint32_t index_bno, ino, inode_block, inode_shift;
	struct buffer_head *ofs_inode_buf, *index_blk_buf;
	struct ouichefs_inode *disk_inode;
	struct ouichefs_inode_info *ci;
	struct ouichefs_file_index_block *new_f_index_blk;
	char *tmp_buf;

	/*allouer nouveau ouichefs_file_index_block */
	ci = OUICHEFS_INODE(file->f_inode);

	/*
	* Si la version courante n'est pas la dernière version
	* alors on empêche l'écriture
	*/
	if (ci->index_block != ci->last_index_block) {
		pr_info("Ecriture sur version anterireur impossible");
		return -1;
	}

	/* Check if the write can be completed (enough space?) */
	if (pos + len > OUICHEFS_MAX_FILESIZE)
		return -ENOSPC;
	nr_allocs = max(pos + len, file->f_inode->i_size) / OUICHEFS_BLOCK_SIZE;
	if (nr_allocs > file->f_inode->i_blocks - 1)
		nr_allocs -= file->f_inode->i_blocks - 1;
	else
		nr_allocs = 0;
	if (nr_allocs > sbi->nr_free_blocks)
		return -ENOSPC;

	/*Exo 1 : allouer nouveau ouichefs_file_index_block*/
	ino = file->f_inode->i_ino;
	inode_block = (ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	inode_shift = ino % OUICHEFS_INODES_PER_BLOCK;

	/* lire le ouichefs inode block du disque */
	ofs_inode_buf = sb_bread(file->f_inode->i_sb, inode_block);
	disk_inode = (struct ouichefs_inode *)ofs_inode_buf->b_data;
	disk_inode += inode_shift;


	/*allouer un nouveau index block pour la nouvelle version*/
	index_bno = get_free_block(sbi);
	if (!index_bno)
		goto get_free_blk_err;

	/* lire le index block cree et l'initialiser / le netoyer */
	index_blk_buf = sb_bread(file->f_inode->i_sb, index_bno);
	tmp_buf = (char *)index_blk_buf->b_data;
	memset(tmp_buf, 0, OUICHEFS_BLOCK_SIZE);
	new_f_index_blk = (struct ouichefs_file_index_block *)tmp_buf;

	/*
	* faire pointer l'inode disque et le inode_info vers le nouveau block
	* et faire pointer le nouveau block vers l'ancienne version
	*/
	new_f_index_blk->prev = ci->index_block;
	ci->index_block = index_bno;
	disk_inode->index_block = index_bno;

	/*
	* Exo 3 : faire pointer l'inode sur le nouveau block avec le champs
	* last_index_block pour avoir une référence sur la dernière version
	* du fichier
	*/

	ci->last_index_block = index_bno;
	disk_inode->last_index_block = index_bno;

	mark_buffer_dirty(ofs_inode_buf);
	mark_buffer_dirty(index_blk_buf);

	brelse(ofs_inode_buf);
	brelse(index_blk_buf);


	/* prepare the write */
	err = block_write_begin(mapping, pos, len, flags, pagep,
				ouichefs_file_get_block);
	/* if this failed, reclaim newly allocated blocks */
	if (err < 0) {
		pr_err("%s:%d: newly allocated blocks reclaim not implemented yet\n",
		       __func__, __LINE__);
	}
	goto ret;
get_free_blk_err:
	pr_err("failed to allocate new file_index_block");
ret:
	return err;
}

/*
 * Called by the VFS after writing data from a write() syscall to the page
 * cache. This functions updates inode metadata and truncates the file if
 * necessary.
 */
static int ouichefs_write_end(struct file *file, struct address_space *mapping,
			      loff_t pos, unsigned int len, unsigned int copied,
			      struct page *page, void *fsdata)
{
	int ret;
	struct inode *inode = file->f_inode;
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct super_block *sb = inode->i_sb;


	/* Complete the write() */
	ret = generic_write_end(file, mapping, pos, len, copied, page, fsdata);

	/* forcer le write de la nouvelle version pour qu'on puisse
	* changer de version avec l'ioctl.
	*/
	filemap_write_and_wait(file->f_mapping);

	if (ret < len) {
		pr_err("%s:%d: wrote less than asked... what do I do? nothing for now...\n",
		       __func__, __LINE__);
	} else {
		uint32_t nr_blocks_old = inode->i_blocks;

		/* Update inode metadata */
		inode->i_blocks = inode->i_size / OUICHEFS_BLOCK_SIZE + 2;
		inode->i_mtime = inode->i_ctime = current_time(inode);
		mark_inode_dirty(inode);

		/* If file is smaller than before, free unused blocks */
		if (nr_blocks_old > inode->i_blocks) {
			int i;
			struct buffer_head *bh_index;
			struct ouichefs_file_index_block *index;

			/* Free unused blocks from page cache */
			truncate_pagecache(inode, inode->i_size);

			/* Read index block to remove unused blocks */
			bh_index = sb_bread(sb, ci->index_block);
			if (!bh_index) {
				pr_err("failed truncating '%s'. we just lost %lu blocks\n",
				       file->f_path.dentry->d_name.name,
				       nr_blocks_old - inode->i_blocks);
				goto end;
			}
			index = (struct ouichefs_file_index_block *)
				bh_index->b_data;

			for (i = inode->i_blocks - 1; i < nr_blocks_old - 1;
			     i++) {
				put_block(OUICHEFS_SB(sb), index->blocks[i]);
				index->blocks[i] = 0;
			}
			mark_buffer_dirty(bh_index);
			brelse(bh_index);
		}
	}
end:
	return ret;
}



/*
* Fonction unlocked_ioctl traitant les requêtes définies pour
* les étapes 3 et 4.
* La requête de l'étape 3 porte le numéro 0 et a pour but de
* modifier la version courante du ficher ciblé par une autre
*/
long ouichefs_unlocked_ioctl(struct file *file, unsigned req,
			      unsigned long param)
{
	long ret = -1, i;
	uint32_t ino, pointed, cur_rest, tmp, inode_shift, inode_block;
	struct inode *inode = file->f_inode;
	struct ouichefs_sb_info *sbi = OUICHEFS_SB(inode->i_sb);
	struct ouichefs_inode_info *ci = OUICHEFS_INODE(inode);
	struct ouichefs_file_index_block *index;
	struct ouichefs_inode *disk_inode;
	struct buffer_head *bh_cur, *ofs_inode_buf;
	int syncok, clearpages;
	/* On récupère l'inode disque */
	ino = inode->i_ino;
	inode_block = (ino / OUICHEFS_INODES_PER_BLOCK) + 1;
	inode_shift = ino % OUICHEFS_INODES_PER_BLOCK;

	ofs_inode_buf = sb_bread(file->f_inode->i_sb, inode_block);
	disk_inode = (struct ouichefs_inode *)ofs_inode_buf->b_data;
	disk_inode += inode_shift;

	pr_debug("----- req COURANTE en traitement -----\n");
	pr_debug("----- Actual version on block %ld ----\n",
				disk_inode->index_block);

	pointed = disk_inode->last_index_block;

	/*
	* on trouve l'index block de la version a param liens
	*  a partir de last_index_block
	*/
	for (i = 0; i < param; i++) {
		bh_cur = sb_bread(inode->i_sb, pointed);
		if (!bh_cur)
			goto err;
		index = (struct ouichefs_file_index_block *)bh_cur->b_data;
		pointed = index->prev;
		brelse(bh_cur);
		/* la version n'existe pas*/
		if (!pointed)
			goto ret;
	}

	ci->index_block = pointed;
	disk_inode->index_block = pointed;
	pr_debug("----- Now actual version on block %ld ----\n",
						disk_inode->index_block);

	ret = pointed;

	if (req == COURANTE)
		goto ret;

	/* RESTAURATION */

	pr_debug("----- req RESTAURATION en traitement -----\n");
	cur_rest = disk_inode->last_index_block;
	/* On effectue un nouveau parcours des index_block de l'inode */
	do {
		bh_cur = sb_bread(inode->i_sb, cur_rest);
		index = (struct ouichefs_file_index_block *)bh_cur->b_data;
		tmp = index->prev;
		/*
		* Si l'index_block est celui de la version que l'on veut
		* alors on fait pointé cette dernière sur 0 avec prev
		*/
		if (cur_rest == pointed) {
			index->prev = 0;
			goto next;
		}
		/* Sinon on libère l'index_block ainsi que les blocs
		* référencées
		*/
		for (i = 0; i < (OUICHEFS_BLOCK_SIZE >> 2) - 1; i++) {
			if (index->blocks[i] == 0)
				break;
			put_block(sbi, index->blocks[i]);
		}
		put_block(sbi, cur_rest);
next:
		cur_rest = tmp;
		brelse(bh_cur);
	} while (cur_rest);

	ci->last_index_block = pointed;
	disk_inode->last_index_block = pointed;

err:
ret:
	mark_buffer_dirty(ofs_inode_buf);
	brelse(ofs_inode_buf);
	syncok = filemap_write_and_wait(file->f_mapping);
	clearpages = invalidate_inode_pages2(file->f_inode->i_mapping);
	pr_debug("invalidate_inode_pages ino %d  writeand wait %d\n",
				syncok, clearpages);
	if (syncok || clearpages) {
		/*a faire*/
		pr_info("%s failed to sync file\n", __func__);
	}
	return ret;
}

const struct address_space_operations ouichefs_aops = {
	.readpage    = ouichefs_readpage,
	.writepage   = ouichefs_writepage,
	.write_begin = ouichefs_write_begin,
	.write_end   = ouichefs_write_end
};

const struct file_operations ouichefs_file_ops = {
	.owner      = THIS_MODULE,
	.llseek     = generic_file_llseek,
	.read_iter  = generic_file_read_iter,
	.write_iter = generic_file_write_iter,
	.unlocked_ioctl = ouichefs_unlocked_ioctl
};
