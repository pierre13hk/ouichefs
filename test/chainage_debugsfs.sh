#!/bin/bash

# On ce placera ici dans le repertoire ou se trouve
# celui monté avec ouichefs et aussi l'exécutable
# user_ioctl

# Teste le chaînage de bloc d'index grâce que debugfs

for i in {0..9} ; do
    echo $i > ouichefs_rep/test_chaine
    cat ouichefs_rep/test_chaine
done

# Dans le cas ou c'est ouichefs_rep est le seul ou premier
# repertoire monté aves ouichefs
cat /sys/kernel/debug/ouichefs/mount-1

# On pourra voir à l'aide cette denière commande les différents
# bloc d'index des versions du fichier test
exit