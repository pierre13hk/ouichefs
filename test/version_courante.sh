#!/bin/bash

# On ce placera ici dans le repertoire ou se trouve
# celui monté avec ouichefs

# Teste le changement de version courante d'un fichier
# et aussi l'impossibilité d'écrire sur une version 
# antérieure

for i in {0..9} ; do
    echo $i > ouichefs_rep/test_courante
done

cat ouichefs_rep/test_courante
# Un 9 s'affiche car c'est la dernière version écrite
./user_ioctl ouichefs_rep/test_courante 0 2
cat ouichefs_rep/test_courante
# Un 7 devrait s'afficher démontrant ainsi que la version
# courante à changé

# Maintenant tentons d'écrire dans le fichier tel quel
echo 10 > ouichefs_rep/test_courante
# Cela renverra une erreur car on ne peut écrire sur une
# version n'étant pas la dernière
exit