#!/bin/bash

# On ce placera ici dans le repertoire ou se trouve
# celui monté avec ouichefs et aussi l'exécutable
# user_ioctl

# Teste la restauration du version antérieure et la
# libération des blocs

for i in {0..9} ; do
    echo $i > ouichefs_rep/test_rest
done

# Ainsi on peut voir les blocs utilisés
cat /sys/kernel/debug/ouichefs/mount-1

# On envoie la requête de restauration pour 3 version précédentes
./user_ioctl ouichefs_rep/test_rest 5 3

# On constate qu'il ne reste que bloc d'index
# correspondant à la version voulue
cat /sys/kernel/debug/ouichefs/mount-1

# Pour vérifier que les blocs on bien été libéré on réécrit 

for i in {7..9} ; do
    echo $i > ouichefs_rep/test_rest
done

# Et on réaffiche les blocs et normalement nous pouvons 
# constater que les blocs des premières écritures ont été
# réutilisé
cat /sys/kernel/debug/ouichefs/mount-1

exit